package uits.jv.test.multilevel.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import uits.jv.test.multilevel.domen.UserDaoImpl;
import uits.jv.test.multilevel.domen.Users;
import uits.jv.test.multilevel.web.util.StringValidator;

@WebServlet(name = "SignUpServlet", urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/WEB-INF/views/signup.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        //validation
        List<String> errors = new ArrayList<>();

        if (StringValidator.isEmpty(login)) {
            errors.add("Login cannot be empty");
        } else {
            if (!StringValidator.isEmailValid(login)) {
                errors.add("Please specify valid login(email)");
            }
        }

        if (StringValidator.isEmpty(password)) {
            errors.add("Password cannot be empty");
        }

        if (StringValidator.isEmpty(firstname)) {
            errors.add("Firstname cannot be empty");
        }
        if (StringValidator.isEmpty(lastname)) {
            errors.add("Lastname cannot be empty");
        }

        if (!errors.isEmpty()) {
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/views/signup.jsp").forward(req, resp);
        } else {

            Users user = new Users();
            user.setFirstname(firstname);
            user.setLastname(lastname);
            user.setLogin(login);
            user.setPassword(password);
            user.setRole(Users.Role.USER);

            new UserDaoImpl().create(user);
            
            send(login, String.format("Hello %s %s", firstname, lastname));
        }

    }

    public static void send(String sendTo, String text) {

        final String username = "blog012345678@gmail.com";
        final String password = "0689629958";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendTo));
            message.setSubject("Welcome");
            message.setText(text);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uits.jv.test.multilevel.domen.UserDaoImpl;
import uits.jv.test.multilevel.domen.Users;

/**
 *
 * @author yulia
 */
@WebServlet(name = "EditServlet", urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        int userid;
        
        try{
            userid = Integer.parseInt(req.getParameter("id"));
        } catch(NumberFormatException e){
            resp.sendError(404, "Such user not found");
            return;
        }
        
        
        Users user = new UserDaoImpl().getById(userid);
        
        
        if (user != null){
            req.setAttribute("user", user);
            req.getRequestDispatcher("/WEB-INF/views/home/edit.jsp").forward(req, resp);
            
        } else {
            resp.sendError(404, "Such user not found");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       int userid;
        
        try{
            userid = Integer.parseInt(req.getParameter("id"));
        } catch(NumberFormatException e){
            resp.sendError(404, "Such user not found");
            return;
        }
        
        Users user = new UserDaoImpl().getById(userid);
        
        
        if (user != null){
            String firstname = req.getParameter("firstname");
             String lastname = req.getParameter("lastname");
             
             if (firstname == null || firstname.isEmpty()){
                 resp.sendError(500, "empty firstname");
                 return;
             }
             
             if (lastname == null || lastname.isEmpty()){
                 resp.sendError(500, "empty lastname");
                 return;
             }
            user.setFirstname(firstname);
            user.setLastname(lastname);
            new UserDaoImpl().update(user);
            resp.sendRedirect("/test-multilevel-web/home");
            return;
            
        } else {
            resp.sendError(404, "Such user not found");
            return;
        }
        
    }

   
}

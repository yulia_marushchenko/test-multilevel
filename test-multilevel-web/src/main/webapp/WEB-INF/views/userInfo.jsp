<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Information</title>
    </head>
    <body>
        <div align="center">
            <!--        <form action="" method="get">-->
            <table BORDER=5>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Login</th>
                    <th>Password</th>
                    <th>Role</th>
                </tr>

                <tr>
                    <td>${userInfo.firstname}</td>
                    <td>${userInfo.lastname}</td>
                    <td>${userInfo.login}</td>
                    <td>${userInfo.password}</td>
                    <td>${userInfo.role}</td>
                </tr>

            </table>
           
            <!--</form>-->
        </div>
                <a href="http://localhost:8080/test-multilevel-web/home" >Back to Home</a> <br>
                <a href="http://localhost:8080/test-multilevel-web/logout" >Logout</a>
    </body>
</html>
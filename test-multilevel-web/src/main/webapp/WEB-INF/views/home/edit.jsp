
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<div id="page-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h2>Users</h2>
                <div class="table-responsive">
                    <form method="POST" action="">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2">Edit</th>

                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>Username</td>
                                    <td>${user.login}</td>
                                </tr>
                                <tr>
                                    <td>Firstname</td>
                                    <td>
                                        <input type="text" name="firstname" value="${user.firstname}"/></td>

                                </tr>
                                <tr>
                                    <td>Lastname</td>
                                    <td><input type="text" name="lastname" value="${user.lastname}"/></td></td>
                                </tr>
                                <tr>
                                    <td><input class="btn btn-success" type="submit" value="Save" name ="Save"/></td>
                                    <td><a href="/test-multilevel-web/home" class="btn btn-warning ">Back</a></td>
                                </tr>

                            </tbody>
                        </table>
                    </form>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
</div>
<%@include file="../commons/footer.jsp" %>
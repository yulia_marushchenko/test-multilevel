CREATE DATABASE IF NOT EXISTS `blog` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE blog;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`(
`id` int (10) unsigned NOT NULL AUTO_INCREMENT,
`firstname` varchar (32) NOT NULL,
`lastname` varchar (32) NOT NULL,
PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts`(
`id` int (10) unsigned NOT NULL AUTO_INCREMENT,
`id_user` int (10) unsigned NOT NULL,
`post` varchar(500) NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY  (`id_user`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_post` int(10) unsigned NOT NULL,
`id_user` int (10) unsigned NOT NULL,
`comment` varchar(255) NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY  (`id_post`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY  (`id_user`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`link` varchar(255) NOT NULL,
`type` enum ('VIDEO','AUDIO','IMAGE') NOT NUll,
`date_created` datetime  NOT NULL default current_timestamp,
`id_user` int(10) unsigned NOT NULL,
PRIMARY KEY(`id`)

);

DROP TABLE IF EXISTS `post_media`;

CREATE TABLE `post_media`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_media` int(10) unsigned NOT NULL,
`id_post` int(10) unsigned NOT NULL,
PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `likes`;

CREATE TABLE `likes`(
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`id_post` int(10) unsigned NOT NULL,
`id_user` int (10) unsigned NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY  (`id_post`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY  (`id_user`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);




 
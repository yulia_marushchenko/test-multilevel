/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.service;

import uits.jv.test.multilevel.domen.BaseDao;
import uits.jv.test.multilevel.domen.Posts;
import uits.jv.test.multilevel.domen.UserDaoImpl;
import uits.jv.test.multilevel.domen.Users;
import uits.jv.test.multilevel.domen.UsersDao;

/**
 *
 * @author yulia
 */
public class UsersService {
    private UsersDao usersDao;
    BaseDao<Posts, Integer> posts;

    public UsersService() {
        usersDao = new UserDaoImpl();
    }

    public UsersService(UsersDao usersDao, BaseDao<Posts, Integer> posts) {
        this.usersDao = usersDao;
        this.posts = posts;
    }
    
    Users getById(Integer id){
        return usersDao.getById(id);
    }
    
    
}

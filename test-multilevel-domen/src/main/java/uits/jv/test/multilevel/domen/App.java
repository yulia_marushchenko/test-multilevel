/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.domen;

/**
 *
 * @author yulia
 */
public class App {
    
    public static void main(String[] args) {
        UsersDao uDao = new UserDaoImpl();
//        System.out.println(uDao.getList());
//        Users user = new Users();
//        user.setFirstname("vasya");
//        user.setLastname("pupkin");
//        uDao.create(user);
//         System.out.println(uDao.getList());

        Users vasya = uDao.getById(1);
//        System.out.println(vasya);
//        System.out.println("posts : ");
//        System.out.println(vasya.getPostsList());

        System.out.println(uDao.getByUserName("vasya"));
        
        Users user1 = new Users();
        user1.setId(1);
        user1.setFirstname("vasya");
        user1.setLastname("pupkin1");
        System.out.println(uDao.update(user1));

        //System.out.println(uDao.update(user1));
        //System.out.println(uDao.remove(user1));
        
        HibernateUtil.getSessionFactory().close();
    }
}

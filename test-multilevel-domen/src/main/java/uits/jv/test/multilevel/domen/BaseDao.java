package uits.jv.test.multilevel.domen;


import java.util.List;


public interface BaseDao <T, I> {
    T getById(I key);
    T create(T o);
    boolean update (T o);
    boolean remove(T o);
    List<T> getList();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.domen;


import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author yulia
 */
public  class UserDaoImpl extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public Users getByUserName(String login) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Users.class).add(Restrictions.eq("login", login));
        List<Users> users = criteria.list();
        session.close();
        return (users.size() > 0) ? users.get(0): null;
    }

//    @Override
//    public Users getById(Integer key) {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        return (Users) session.get(Users.class, key);
//    }
//
//    @Override
//    public Users create(Users o) {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.save(o);
//            t.commit();
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot save user", e);
//        }
//
//        return o;
//    }
//
//    @Override
//    public boolean update(Users o) {
//        boolean updated = false;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.merge("id", o);
//            t.commit();
//            updated = true;
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot update user", e);
//        }
//        return updated;
//    }
//
//    @Override
//    public boolean remove(Users o) {
//        boolean removed = false;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = session.beginTransaction();
//        try {
//            session.delete(o);
//            t.commit();
//            removed = true;
//        } catch (Exception e) {
//            t.rollback();
//            throw new RuntimeException("Cannot update user", e);
//        }
//        return removed;
//    }
//
//    @Override
//    public List<Users> getList() {
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        return session.createCriteria(Users.class).list();
//    }
    
}

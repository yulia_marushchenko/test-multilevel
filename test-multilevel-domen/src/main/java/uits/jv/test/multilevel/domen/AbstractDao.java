/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.test.multilevel.domen;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author yulia
 */
public abstract class AbstractDao<T, I extends Serializable> implements BaseDao<T, I> {

    private final Class<T> entityClass;
    

    protected Session getCurrentSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    public AbstractDao() {
        ParameterizedType genericSuperClass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperClass.getActualTypeArguments()[0];
    }

    @Override
    public T getById(I key) {
        Session session = getCurrentSession();
        return (T) session.load(entityClass, key);

    }

    @Override
    public T create(T o) {
        Session session = getCurrentSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(o);
            t.commit();
            session.setFlushMode(FlushMode.AUTO);
        } catch (Exception e) {
            t.rollback();
            session.setFlushMode(FlushMode.AUTO);
            throw new RuntimeException("Cannot save " + entityClass.toString(), e);
        }

        return o;
    }

    @Override
    public boolean update(T o) {
        boolean updated = false;
        Session session = getCurrentSession();
        Transaction t = session.beginTransaction();
        try {
            //session.update(o);
            session.merge(o);
            t.commit();
            session.setFlushMode(FlushMode.AUTO);
            updated = true;
        } catch (Exception e) {
            e.printStackTrace();
            t.rollback();
            session.setFlushMode(FlushMode.AUTO);
            throw new RuntimeException("Cannot update " + entityClass.toString(), e);
        }
        return updated;
    }

    @Override
    public boolean remove(T o) {
        boolean removed = false;
        Session session = getCurrentSession();
        Transaction t = session.beginTransaction();
        try {
            session.delete(o);
            t.commit();
            removed = true;
        } catch (Exception e) {
            t.rollback();
            throw new RuntimeException("Cannot delete " + entityClass.toString(), e);
        }
        return removed;
    }

    @Override
    public List<T> getList() {
        Session session = getCurrentSession();
        List list = session.createCriteria(entityClass).list();
        session.setFlushMode(FlushMode.AUTO);
        return list;
    }

}
